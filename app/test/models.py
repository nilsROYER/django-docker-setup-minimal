from django.db import models

# Create your models here.
class Testdata(models.Model):
    question_text = models.CharField(max_length=250)
    pub_date = models.DateTimeField()

class SecondTest(models.Model):
    second_test_text = models.CharField(max_length=250)
    second_test_int = models.IntegerField(default=0)