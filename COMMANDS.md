### Commande docker:

## les Images:
# docker hub :
envoyer une image sur le hub:
```sh
docker push [images name]:[image tag]
```

récupérer une image du hub
```sh
docker pull [image]
```

# Manipuler ses images:
voir ses image
```sh
docker images
```

supprimer une image:
```sh
docker rm [nom ou id de mon image]
```

## Les conteneurs:

builder une image
```sh
docker build --tags=[montag] [chemin de mon dockerfile]
```

instancié un conteneur 
```sh
docker run [repos:imagetag] 
```

voir c'est conteneur actif
```sh
docker ps
```

voir tout les conteneur monter ou non 
```sh
docker ps -a
```

démarrer un conteneur:
```sh
docker start [nomduconteur ou id]
```

arréter un conteneur 
```sh
docker stop [nomconteneur ou id] 
```

mettre en pause le conteneur
```sh
docker pause 
```

remais en marche le conteneur
```sh
docker unpause
```

supprimer 1 conteneur:
```sh
docker rm [nom ou id du conteneur]
```

supprimer tout les conteneurs:
```sh
docker container prune
```

suprimer toutes les images:
```sh
docker image prune --all
```
    
### docker-compose:
!!! les commande docker-compose ne peuvent s'éxecuter que si on a un fichier docker-compose.yml

builder les services: 
(les service sont les differente images présente dans le docker compose)
```sh
docker-compose build
```

monter les conteneurs:
!!! builder les images avant
```sh
docker-compose up
```

monter les service en mode détacher:
```sh
docker-compose up -d
```

rebuilder les images et monter les conteneurs:
```sh
docker-compose up --build
```

voir tout c'est services:
```sh
docker-compose ps
```
stoper tout les service:
```sh
dockr-compose stop 
```

démarrer tous les services:
```sh
docker-compose start 
```

démonter tout les services:
```sh
docker-compose down 
```

voir les logs de ses conteneur en tache de fond
```bash
docker-compose logs 
```

rentrer dans un conteneur
```bash
docker-compose exec [monconteneur] sh
```


